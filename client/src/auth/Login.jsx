// import React, { useState} from 'react';
// import { NavLink } from 'react-router-dom';
// import logo from '../assets/logo.png';
// // import { Link } from 'react-router-dom';
// import { useNavigate} from 'react-router-dom';
// import axios from 'axios';
// import { toast, ToastContainer } from "react-toastify";
// import "react-toastify/dist/ReactToastify.css";


// import './Auth.css';
// const Login = () => {
//   const navigate = useNavigate();
//   const [email, setEmail] = useState("");
//   const [password, setPassword] = useState("");
//   const [isLoggedIn, setIsLoggedIn] = useState(false);

//   const handleSubmitLogin = async (e) => {
//     e.preventDefault();

//     try {
//       // Replace with your actual server endpoint for login
//       const response = await axios.post("http://localhost:5000/login", {
//         email,
//         password,
//       });
//       if (response.data.success){
        
//       }
//       const { token} = response.data;

//       // Store the token in localStorage
//       localStorage.setItem("jwtToken", token);

//        // Set isLoggedIn state to true upon successful login
//        setIsLoggedIn(true);

//       toast.success('Logged in successfully!');
//       // Perform a full-page refresh
//       navigate("/dashboard", {
//         state: {
//           successMessage: "Logged in successfully.",
//         },
//       });
//     } catch (error) {
//       console.error("Login failed", error.response?.data);

//       // Display an error toast to the user
//       toast.error(
//         error.response?.data?.message ||
//           "Login failed. Please check your email and password."
//       );
//     }
//   };

//   return (
//     <div className='login-container'>
//        <div className="container">
//           <h1 className='text-white fw-bold pt-5'>Log In</h1>
//           <div className="row">
//              <div className="col-md-8">
//               <div className='h-100 d-md-flex d-none justify-content-center align-items-center'>
//                 <NavLink style={{height:'60px',}} className="d-flex text-decoration-none gap-2" to="/">
//                         <img style={{width:'100px',height:'100px',objectFit:'contain'}} src={logo} alt="logo" />
//                         <h5 style={{fontSize:'40px'}} className='text-white fw-bold pt-3'>PORTFOLIO WEBSITE</h5>
//                 </NavLink>
//               </div>
//              </div>
//              <div className="col-md-4">
//                <div className='bg-white bg-opacity-75 rounded-4 p-5' style={{minHeight:'500px'}}>
//                     <h1 className='fw-bold'>Welcome Back</h1>
//                     <form onSubmit={handleSubmitLogin}>
//                       <div class="mb-3 mt-3">
//                           <label  className="form-label">Email address</label>
//                           <input required type="email" placeholder='mr/mrsxx@gmail.com' className="form-control bg-transparent border border-dark rounded-5" value={email} onChange={(e) => setEmail(e.target.value)}/>
//                         </div>
//                         <div className="mb-3">
//                           <label  className="form-label">Password</label>
//                           <input required type="password" className="form-control bg-transparent border border-dark rounded-5" value={password} onChange={(e) => setPassword(e.target.value)}/>
//                         </div>
//                         <div className='d-flex mt-4 justify-content-center align-items-center'>
//                           <button type="submit" className="btn btn-secondary text-white fw-bold fs-5 rounded-5 px-4">Log In</button>
//                         </div>
//                     </form>
//                     <p className='mt-5 text-center text-uppercase'>DON’T have an account? <NavLink to="/register">SIGN UP</NavLink></p>
//                 </div>
//              </div>
//           </div>
//        </div>
//        <ToastContainer
//         position="top-center"
//         autoClose={5000}
//         hideProgressBar={false}
//         newestOnTop={false}
//         closeOnClick
//         rtl={false}
//         pauseOnFocusLoss
//         draggable
//         pauseOnHover
//         theme="colored"
//     />
//     </div>
//   )
// }

// export default Login

import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import logo from '../assets/logo.png';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import './Auth.css';

const Login = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmitLogin = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post("http://localhost:5000/login", {
        email,
        password,
      });
      const { token } = response.data;

      localStorage.setItem("jwtToken", token);
      toast.success('Logged in successfully!');
      navigate("/dashboard", {
        state: {
          successMessage: "Logged in successfully.",
        },
      });
    } catch (error) {
      console.error("Login failed", error.response?.data);
      toast.error(
        error.response?.data?.message ||
        "Login failed. Please check your email and password."
      );
    }
  };

  return (
    <div className='login-container'>
      <div className="container">
        <h1 className='text-white fw-bold pt-5'>Log In</h1>
        <div className="row">
          <div className="col-md-8">
            <div className='h-100 d-md-flex d-none justify-content-center align-items-center'>
              <NavLink style={{ height: '60px' }} className="d-flex text-decoration-none gap-2" to="/">
                <img style={{ width: '100px', height: '100px', objectFit: 'contain' }} src={logo} alt="logo" />
                <h5 style={{ fontSize: '40px' }} className='text-white fw-bold pt-3'>PORTFOLIO WEBSITE</h5>
              </NavLink>
            </div>
          </div>
          <div className="col-md-4">
            <div className='bg-white bg-opacity-75 rounded-4 p-5' style={{ minHeight: '500px' }}>
              <h1 className='fw-bold'>Welcome Back</h1>
              <form onSubmit={handleSubmitLogin}>
                <div class="mb-3 mt-3">
                  <label className="form-label">Email address</label>
                  <input required type="email" placeholder='mr/mrsxx@gmail.com' className="form-control bg-transparent border border-dark rounded-5" value={email} onChange={(e) => setEmail(e.target.value)} />
                </div>
                <div className="mb-3">
                  <label className="form-label">Password</label>
                  <input required type="password" className="form-control bg-transparent border border-dark rounded-5" value={password} onChange={(e) => setPassword(e.target.value)} />
                </div>
                <div className='d-flex mt-4 justify-content-center align-items-center'>
                  <button type="submit" className="btn btn-secondary text-white fw-bold fs-5 rounded-5 px-4">Log In</button>
                </div>
              </form>
              <p className='mt-5 text-center text-uppercase'>DON’T have an account? <NavLink to="/register">SIGN UP</NavLink></p>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
      />
    </div>
  )
}

export default Login;
