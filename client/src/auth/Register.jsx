import React, { useState} from 'react'
import { NavLink } from 'react-router-dom';
import logo from '../assets/logo.png';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Register = () => {
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [profession, setProfession] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [nameError, setNameError] = useState('');
  const [professionError, setProfessionError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');
  const navigate = useNavigate();

  const handleEmailChange = (e) => {
            const value = e.target.value;
            setEmail(value);
            setEmailError('');
    
            // Validate email format
            if (!value) {
              setEmailError('Email cannot be empty.');
          }
        };
    
        const handleNameChange = (e) => {
          const value = e.target.value;
          setName(value);
          setNameError('');
      
          // Validate name format
          if (!/^[a-zA-Z\s]+$/.test(value)) {
              setNameError('Name format is invalid. It should contain only letters and spaces.');
          }
      };
    
      const handleProfessionChange = (e) => {
        const value = e.target.value;
        setProfession(value);
        setProfessionError('');
    
        // Validate name format
        if (!value) {
          setProfessionError('Profession cannot be empty.');
      }
    };
    
        const handlePasswordChange = (e) => {
            const value = e.target.value;
            setPassword(value);
            setPasswordError('');
    
            // Validate password format
            if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()])[A-Za-z\d!@#$%^&*()]{8,15}$/.test(value)) {
                setPasswordError('Password must be at least 8 characters long, maximum 15 characters, containing at least one uppercase letter, one lowercase letter, one number, and one special character.');
            }
        };
    
        const handleConfirmPasswordChange = (e) => {
            setConfirmPassword(e.target.value);
            setConfirmPasswordError('');
        };
    
        const handleSubmit = async (e) => {
          e.preventDefault();
      
          // Validation
          if (!name || !email || !profession || !password || !confirmPassword) {
              toast.error('All fields are required.');
              return;
          }
      
          if (!/^[a-zA-Z\s]+$/.test(name)) {
              setNameError('Name format is invalid. It should contain only letters and spaces.');
              return;
          }
      
          if (password !== confirmPassword) {
              setConfirmPasswordError('Passwords do not match');
              return;
          }
      
          if (emailError || passwordError || confirmPasswordError) {
              toast.error('Please fix the errors before submitting.');
              return;
          }
      
          try {
              const response = await axios.post('http://localhost:5000/register', {
                  name,
                  email,
                  profession,
                  password,
              });
      
              if (response.data.success) {
                  const { token, user } = response.data;
      
                  localStorage.setItem("jwtToken", token);
                  localStorage.setItem("userType", user.usertype);
      
                  window.location.reload();
                  // Redirect to home page
                  navigate("/home", {
                      state: {
                          successMessage: "User registered successfully.",
                      }
                  });
              } else {
                  toast.error(response.data.message || "Registration failed. Please try again.");
                  console.error("Invalid response format", response);
              }
          } catch (error) {
              if (error.response) {
                  toast.error(`Registration failed: ${error.response.data.message}`);
                  console.error("Registration failed", error.response.data);
              } else {
                  toast.error("Registration failed. Please try again.");
                  console.error("Unexpected error", error);
              }
          }
      };

  return (
    <div className='signup-container'>
       <div className="container">
          <h1 className='text-white fw-bold pt-5'>Sign Up</h1>
          <div className="row">
             <div className="col-md-8">
              <div className='h-100 d-md-flex d-none justify-content-center align-items-center'>
                <NavLink style={{height:'60px',}} className="d-flex text-decoration-none gap-2" to="/">
                        <img style={{width:'100px',height:'100px',objectFit:'contain'}} src={logo} alt="logo" />
                        <h5 style={{fontSize:'40px'}} className='text-white fw-bold pt-3'>PORTFOLIO WEBSITE</h5>
                </NavLink>
              </div>
             </div>
             <div className="col-md-4">
               <div className='bg-white bg-opacity-75 rounded-4 p-5 mb-3' style={{minHeight:'500px'}}>
                    <h1 className='fw-bold'>Create Account</h1>
                    <form onSubmit={handleSubmit}>
                       <div class="mb-3 mt-3">
                           <label  className="form-label">Email address</label>
                          <input required type="email" className="form-control bg-transparent border border-dark rounded-5" value={email} onChange={handleEmailChange}/>
                        </div>
                         {emailError && <p className="error-message">{emailError}</p>}

                         <div class="mb-3 mt-3">
                           <label  className="form-label">Name</label>
                           <input required type="text" className="form-control bg-transparent border border-dark rounded-5" value={name} onChange={handleNameChange}/>
                         </div>
                         {nameError && <p className="error-message">{nameError}</p>}

                         <div class="mb-3 mt-3">
                           <label  className="form-label">Profession</label>
                           <input required type="text" className="form-control bg-transparent border border-dark rounded-5" value={profession} onChange={handleProfessionChange}/>
                         </div>
                         {professionError && <p className="error-message">{professionError}</p>}

                         <div className="mb-3">
                           <label  className="form-label">Password</label>
                           <input required type="password" className="form-control bg-transparent border border-dark rounded-5" value={password} onChange={handlePasswordChange}/>
                         </div>
                        {passwordError && <p className="error-message">{passwordError}</p>}

                         <div className="mb-3">                           <label  className="form-label">Confirm Password</label>
                         <input required type="password" className="form-control bg-transparent border border-dark rounded-5" value={confirmPassword} onChange={handleConfirmPasswordChange}/>
                       </div>
                       {confirmPasswordError && <p className="error-message">{confirmPasswordError}</p>}

                        <div className='d-flex mt-4 justify-content-center align-items-center'>
                          <button type="submit" className="btn btn-secondary text-white fw-bold fs-5 rounded-5 px-4">Sign Up</button>
                         </div>
                     </form>
                    <p className='mt-5 text-center text-uppercase'>already have an account? <NavLink to="/login">SIGN In</NavLink></p>
                </div>
             </div>
          </div>
       </div>
       {/* <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            /> */}
    </div>
  )
}

export default Register
