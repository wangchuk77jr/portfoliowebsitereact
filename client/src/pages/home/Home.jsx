import React, { useEffect, useState } from 'react';
import axios from 'axios';
import GuestLayout from '../../layouts/GuestLayout';
import HomeHeroBanner from '../../components/HomeHeroBanner';
import { NavLink } from 'react-router-dom';

const Home = () => {
  const isAuthenticated = localStorage.getItem("jwtToken") !== null;
  const [project, setProject] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredProject, setFilteredProject] = useState([]);

  const fetchProject = async () => {
    try {
      const response = await axios.get(`http://localhost:5000/api/getProject`);
      setProject(response.data.project);
      setFilteredProject(response.data.project); // Initialize filteredProject with all project data
    } catch (error) {
      console.error('Error fetching project data:', error);
    }
  };

  useEffect(() => {
    fetchProject();
  }, []);

  const handleSearch = (e) => {
    const query = e.target.value;
    setSearchQuery(query);
    const filtered = project.filter(item =>
      item.name.toLowerCase().includes(query.toLowerCase()) ||
      item.author.toLowerCase().includes(query.toLowerCase())
    );
    setFilteredProject(filtered);
  };

  return (
    <GuestLayout herobannerChildren={<HomeHeroBanner/>}>
      <div style={{ width: '80%' }} className='container mt-3'>
        <div className="row">
          <div className="col">
            <form action="">
              <div className="search-group">
                <input
                  type="search"
                  placeholder="Search the creative work"
                  className="form-control h-100 ps-5 user-search-input rounded-5 bg-body-secondary border border-info"
                  value={searchQuery}
                  onChange={handleSearch}
                />
                <span className="user-search-input-icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style={{ fill: 'rgba(0, 0, 0, 1)' }}>
                    <path d="M10 18a7.952 7.952 0 0 0 4.897-1.688l4.396 4.396 1.414-1.414-4.396-4.396A7.952 7.952 0 0 0 18 10c0-4.411-3.589-8-8-8s-8 3.589-8 8 3.589 8 8 8zm0-14c3.309 0 6 2.691 6 6s-2.691 6-6 6-6-2.691-6-6 2.691-6 6-6z"></path>
                  </svg>
                </span>
              </div>
            </form>
          </div>
        </div>
        <div className="row mt-4 g-md-4 g-2">
          {filteredProject.length > 0 ? (
            filteredProject.map((item, index) => (
              <div className="col-md-3" key={index}>
                <NavLink className='d-block text-decoration-none' to={{ pathname: '/profileView', search: `?user_id=${item.user_id}` }}>
                  <img
                    style={{ height: '200px', objectFit: 'cover' }}
                    className='w-100'
                    src={`http://localhost:5000/uploads/${encodeURIComponent(item.upload)}`}
                    alt="Project Image"
                  />
                  <h5 className='mt-3'>{item.name}</h5>
                  <p className='mt-3'>{item.author}</p>
                </NavLink>
              </div>
            ))
          ) : (
            <p>No results found.</p>
          )}
        </div>

        {!isAuthenticated && (
            <div className="container">
              <div className="row mt-4 mb-5">
                  <h5 className="text-center w-100 fw-bold">
                    <NavLink to="/login">Sign In</NavLink> or <NavLink to="/register">Sign Up</NavLink> to your account
                  </h5>
              </div>
            </div>
        )}
      </div>
    </GuestLayout>
  );
};

export default Home;