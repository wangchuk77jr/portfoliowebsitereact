import React from 'react'
import logo from '../../assets/logo.png'
import { NavLink,useNavigate } from "react-router-dom";
import './Dashboard.css'
import ProfileHome from '../../components/dashboard/ProfileHome';
import About from '../../components/dashboard/About';
import Resume from '../../components/dashboard/Resume';
import Portfolio from '../../components/dashboard/Portfolio';
export const Dashboard = () => {
  const navigate = useNavigate();
  const handleLogout = () => {
    localStorage.clear();
    window.location.reload();
    navigate('/login');
  };

  return (
    <>
        {/* Nav bar section */}
        <nav style={{zIndex:'999'}} className="navbar navbar-expand-lg bg-primary py-3 position-sticky top-0">
          <div className="container-fluid px-md-5 px-3">
          <div className='nav-bar-items me-md-5'>
                <NavLink style={{height:'50px',}} className="navbar-brand  d-flex align-items-center justify-content-center gap-2" to="/">
                    <img src={logo} alt="logo" />
                    <span className='text-white fw-bold pt-2 logo-text'>PORTFOLIO WEBSITE</span>
                </NavLink>
            </div>
            <button className="navbar-toggler bg-white" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav profile-nav ms-md-5 gap-md-5">
                <li className="nav-item">
                  <a className="nav-link text-white" aria-current="page" href="#profile">Home</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-white" href="#about">About</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-white" href="#resume">Resume</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-white" href="#portfolio">Portfolio</a>
                </li>
              </ul>
              <ul className="navbar-nav profile-nav ms-auto">
                <li className="nav-item">
                    <button onClick={handleLogout} className='btn btn-secondary text-white rounded-5 px-3'>Log out</button>
                </li>
              </ul>
            </div>
          </div>
      </nav>
      <div id='profile' style={{minHeight:'90vh'}}>
         <ProfileHome/>
      </div>
      <div id='about' style={{minHeight:'10vh'}}>
         <About/>
      </div>
      <div id='resume' style={{minHeight:'10vh'}}>
         <Resume/>
      </div>
      <div id='portfolio' style={{minHeight:'10vh'}}>
        <Portfolio/>
      </div>

      {/* Footer */}
      <h4 className='text-center m-0 bg-primary text-white py-3'>© Copyright. All Rights Reserved</h4>
    </>
  )
}
