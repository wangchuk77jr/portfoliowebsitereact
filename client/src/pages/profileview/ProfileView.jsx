import React from 'react'
import './profileView.css';
import logo from '../../assets/logo.png'
import { NavLink } from 'react-router-dom'
import ProfileHome from '../../components/profileView/ProfileHome';
import About from '../../components/profileView/About';
import Resume from '../../components/profileView/Resume';
import Portfolio from '../../components/profileView/Portfolio';
import Contact from '../../components/profileView/Contact';
const ProfileView = () => {
  return (
    <>
       {/* Nav bar section */}
       <nav style={{zIndex:'999'}} className="navbar navbar-expand-lg bg-primary py-3 position-sticky top-0">
          <div className="container-fluid px-md-5 px-3">
          <div className='nav-bar-items me-md-5'>
                <NavLink style={{height:'50px',}} className="navbar-brand  d-flex align-items-center justify-content-center gap-2" to="/">
                    <img src={logo} alt="logo" />
                    <span className='text-white fw-bold pt-2 logo-text'>PORTFOLIO WEBSITE</span>
                </NavLink>
            </div>
            <button className="navbar-toggler bg-white" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav profile-nav ms-md-5 gap-md-5">
                <li className="nav-item">
                  <a className="nav-link text-white" aria-current="page" href="#profile">Home</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-white" href="#about">About</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-white" href="#resume">Resume</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-white" href="#portfolio">Portfolio</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-white" href="#contact">Contact</a>
                </li>
              </ul>
            </div>
          </div>
      </nav>
      <div id='profile' style={{minHeight:'90vh'}}>
         <ProfileHome/>
      </div>
      <div id='about' style={{minHeight:'90vh'}}>
         <About/>
      </div>
      <div id='resume' style={{minHeight:'90vh'}}>
         <Resume/>
      </div>
      <div id='portfolio' style={{minHeight:'90vh'}}>
        <Portfolio/>
      </div>
      <div id='contact' style={{minHeight:'90vh'}}>
         <Contact/>
      </div>

      {/* Footer */}
      <h4 className='text-center m-0 bg-primary text-white py-3'>© Copyright. All Rights Reserved</h4>
    </>
  )
}

export default ProfileView