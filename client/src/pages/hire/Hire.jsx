import React, { useEffect, useState } from 'react';
import axios from 'axios';
import GuestLayout from '../../layouts/GuestLayout';
import HireHeroBanner from '../../components/HireHeroBanner';
import { NavLink } from 'react-router-dom';

const Hire = () => {
const [hire, setHire] = useState([]);
const [searchQuery, setSearchQuery] = useState('');
const [filteredHire, setFilteredHire] = useState([]);
const [loading, setLoading] = useState(false); // Add loading state

const fetchHire = async () => {
    setLoading(true); // Set loading to true before fetching data
    try {
        const response = await axios.get(`http://localhost:5000/api/getHire`);
        setHire(response.data.hire);
        setFilteredHire(response.data.hire); // Initialize filteredHire with all data
    } catch (error) {
        console.error('Error fetching project data:', error);
    } finally {
        setLoading(false); // Set loading to false after fetching data
    }
};

useEffect(() => {
    fetchHire();
}, []);

const handleSearch = (e) => {
    const query = e.target.value.toLowerCase(); // Convert query to lowercase once
    setSearchQuery(query);
    const filtered = hire.filter(item =>
        (item.name && item.name.toLowerCase().includes(query)) || // Add null check
        (item.author && item.author.toLowerCase().includes(query)) // Add null check
    );
    setFilteredHire(filtered);
};
  
  return (
    <GuestLayout herobannerChildren={<HireHeroBanner searchQuery={searchQuery} onSearch={handleSearch}/>}>
        <div style={{width:'80%',}} className='container mt-3'>
        <h5 className='fw-bold mb-3'>1000+ Results</h5>
        <div className="row g-3 mb-5">
                {loading ? <p>Loading...</p> : (
                    filteredHire.length > 0 ? (
                        filteredHire.map((item, index) => (
                            
                    <div className="col-md-4 ">
                            <div key={index} className='position-relative card border border-dark' style={{ flex: '0 0 auto' }}>
                                <img 
                                style={{ height: '8rem', objectFit: 'cover', objectPosition: 'center' }}
                                src="https://images.unsplash.com/photo-1581345331960-d1b0a223ef96?q=80&w=1753&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                                className="m-2 rounded"
                                alt="backgroundImg"/>
                                <img style={{width:'100px',height:'100px',top:'7rem',objectFit:'cover',objectPosition:'center'}} className="rounded-circle position-absolute start-50 translate-middle border border-info" 
                                src={`http://localhost:5000/uploads/${encodeURIComponent(item.profile_bg)}`} alt="profileImg" />
                                <div className="card-body mt-4 px-4 pb-5">
                                    <h4 className="card-title text-center fw-bold">{item.name}</h4>
                                    <h4 className='text-center fw-bold opacity-75'>{item.profession}</h4>
                                    <p className="text-center">
                                        <span className='border border-dark-subtle rounded-5 d-inline-block px-3 text-secondary'>
                                        {item.complete} Projects Completed   
                                        </span>
                                    </p>
                                    <NavLink to={{ pathname: '/profileView', search: `?user_id=${item.id}` }} className="border btn-hire border-dark-subtle rounded-5 d-block px-3 py-2 text-info text-center text-decoration-none">
                                        Hire {item.name}
                                    </NavLink>
                                </div>
                            </div>
                            </div>
                            
                        ))
                    ) : (
                        <p>No results found.</p>
                    )
                    
                )} </div>
            </div>
    </GuestLayout>
  )
}

export default Hire