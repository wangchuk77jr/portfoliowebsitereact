import React, { useEffect, useState } from 'react';
import axios from 'axios';
import GuestLayout from '../../layouts/GuestLayout';
import DiscoverHeroBanner from '../../components/DiscoverHeroBanner'
import { NavLink } from 'react-router-dom';

const Discover = () => {
  const isAuthenticated = localStorage.getItem("jwtToken") !== null;
  const [project, setProject] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredProject, setFilteredProject] = useState([]);

  const fetchProject = async () => {
    try {
      const response = await axios.get(`http://localhost:5000/api/getProject`);
      setProject(response.data.project);
      setFilteredProject(response.data.project); // Initialize filteredProject with all project data
    } catch (error) {
      console.error('Error fetching project data:', error);
    }
  };

  useEffect(() => {
    fetchProject();
  }, []);

  const handleSearch = (e) => {
    const query = e.target.value;
    setSearchQuery(query);
    const filtered = project.filter(item =>
      item.name.toLowerCase().includes(query.toLowerCase()) ||
      item.author.toLowerCase().includes(query.toLowerCase())
    );
    setFilteredProject(filtered);
  };

  return (
    <GuestLayout herobannerChildren={<DiscoverHeroBanner searchQuery={searchQuery} onSearch={handleSearch} />} >
      <div style={{width:'80%',}} className='container mt-3'>
          {/* Portfolio Section */}
          <div className="row mt-4 g-md-4 g-2">
              <div className="row mt-4 g-md-4 g-2">
                        {filteredProject.length > 0 ? (
                          filteredProject.map((item, index) => (
                            <div className="col-md-3" key={index}>
                              <NavLink className='d-block text-decoration-none' to={{ pathname: '/profileView', search: `?user_id=${item.user_id}` }}>
                                <img
                                  style={{ height: '200px', objectFit: 'cover' }}
                                  className='w-100'
                                  src={`http://localhost:5000/uploads/${encodeURIComponent(item.upload)}`}
                                  alt="Project Image"
                                />
                                <h5 className='mt-3'>{item.name}</h5>
                                <p className='mt-3'>{item.author}</p>
                              </NavLink>
                            </div>
                          ))
                        ) : (
                          <p>No results found.</p>
                        )}
              </div>
          </div>

          {!isAuthenticated && (
            <div className="row mt-4 mb-5">
              <h5 className='text-center w-100 fw-bold'>
                <NavLink to="/login">Sign In</NavLink> or <NavLink to="/register">Sign Up</NavLink> to your account
              </h5>
            </div>
           )}
      </div>
    </GuestLayout>
  )
}

export default Discover