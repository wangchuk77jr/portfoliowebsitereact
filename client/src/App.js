import React from "react";
import { BrowserRouter as Router, Routes, Route,Navigate } from 'react-router-dom'
import PrivateRoutes from "./utils/PrivateRoutes";
// Page components
import Login from "./auth/Login";
import Register from "./auth/Register";
import Home from "./pages/home/Home";
import Discover from "./pages/discover/Discover";
import Hire from "./pages/hire/Hire";
import ProfileView from "./pages/profileview/ProfileView";
import { Dashboard } from "./pages/dashboard/Dashboard";

function App() {
  const isAuthenticated = localStorage.getItem("jwtToken") !== null;

  return (
    <>
      <Router>
          <Routes>
            <Route element={<PrivateRoutes isAuthenticated={isAuthenticated} />}>
                <Route element={<Dashboard/>} path="/dashboard"/>
            </Route>
            <Route element={<Home/>} path="/" exact/>
            <Route element={<Discover/>} path="/discover"/>
            <Route element={<Hire/>} path="/hire"/>
            <Route element={<ProfileView/>} path="/profileView"/>
             {/* Redirect authenticated users from login and signup pages */}
            {isAuthenticated && (
              <>
                <Route path="/login" element={ <Navigate to="/dashboard" />} />
                <Route path="/register" element={<Navigate to="/dashboard" />} />
              </>
            )}

            {/* Route to login and signup pages for unauthenticated users */}
            {!isAuthenticated && (
              <>
                <Route path="/login" element={<Login />} />
                <Route path="/register" element={<Register />} />
              </>
            )}

          </Routes>
      </Router>
    </>
  );
}

export default App;



