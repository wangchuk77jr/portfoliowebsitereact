import React from 'react'
import GeneralNavbar from '../components/GeneralNavbar';
import './GeneralLayout.css'
import logo from '../assets/logo.png'
import { NavLink } from 'react-router-dom'

const GuestLayout = ({children,herobannerChildren}) => {
  return (
    <div>
        <header className='container-fluid  p-0 header-section'>
            <GeneralNavbar/>
            {/* Hero Banner Content */}
            <div className='mt-5'>
                {herobannerChildren}
            </div>
        </header>
        <main className='min-vh-100'>
            {children}
        </main>
        <footer className='bg-primary'>
            <div className="container-fluid px-md-5 px-3">
              <div className="row g-3  pt-4 pb-2">
                <div className="col-md-6">
                  <div>
                    <NavLink style={{height:'60px',}} className="d-flex gap-2" to="/">
                        <img src={logo} alt="logo" />
                        <h5 className='text-white fw-bold pt-3'>PORTFOLIO WEBSITE</h5>
                    </NavLink>
                  </div>
                </div>
                <div className="col-md-6">
                   <div className='d-flex gap-md-5 gap-3 flex-wrap'>
                      <NavLink className="fw-bold text-uppercase text-decoration-none text-white py-3" to="/">Face Book</NavLink>
                      <NavLink className="fw-bold text-uppercase text-decoration-none text-white py-3" to="/">Intagram</NavLink>
                      <NavLink className="fw-bold text-uppercase text-decoration-none text-white py-3" to="/">whatsapp</NavLink>
                   </div>
                </div>
              </div>
              <div className="row">
                <div className='text-center text-white w-100 mt-3'>
                    © 2023 PORTFOLIO | Designed By GROUP 6 All rights reserved.
                </div>
              </div>
            </div>
        </footer>
    </div>
  )
}

export default GuestLayout