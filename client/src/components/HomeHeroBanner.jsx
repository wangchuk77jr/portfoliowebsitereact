import React from 'react'
import share from '../assets/share.png'
import career from '../assets/career.png'
import nu from '../assets/nu.png'
const HomeHeroBanner = () => {
  return (
    <div className='container'>
         <h1 className='text-center fw-bold text-white'>Showcase Your Work & Get Paid</h1>
         <h6 className='text-center fw-bold text-white'>Join PORTFOLIO WEBSITE, the world's largest creative network</h6>
         <div className='row g-3 justify-content-center gap-md-5 gap-1 flex-wrap mt-4 pb-3'>
             <div className='bg-body-secondary py-4 rounded-5 px-3 col-md-2'>
                <img src={share} alt="" />
                <span className='fw-bold'> Share your work.</span>
             </div>
             <div className='bg-body-secondary py-4 rounded-5 px-3 col-md-2'>
                <img src={career} alt="" />
                <span className='fw-bold'> Grow your career.</span>
             </div>
             <div className='bg-body-secondary py-4 rounded-5 px-3 d-flex gap-2 col-md-2'>
                <img style={{objectFit:'contain'}} className='img-fluid' src={nu} alt="nu" />
                <span className='fw-bold'> Get HIRED and <br /> paid.</span>
             </div>
         </div>
    </div>
  )
}

export default HomeHeroBanner