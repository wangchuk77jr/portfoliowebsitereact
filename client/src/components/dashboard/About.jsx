import React, { useState, useEffect } from 'react';
import axios from "axios";
import { jwtDecode } from 'jwt-decode';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const About = () => {
  const [user_Id, setUser_Id] = useState('');
  const [aboutme, setAboutMe] = useState([]);

  const [formData, setFormData] = useState({
    name: '',
    phone: '',
    city: '',
    age: '',
    degree: '',
    html: '',
    js: '',
    css: '',
    freelance: 'available',
    profile_image: null
  });

useEffect(() => {
    const token = localStorage.getItem('jwtToken');
    if (token) {
      const decodedToken = jwtDecode(token);
      const userId = decodedToken.user.id;
      setUser_Id(userId);

      // fetch data
      const fetchAboutMe = async () => {
        try {
            const response = await axios.get(`http://localhost:5000/api/getAboutme/${userId}`);
            setAboutMe(response.data);
        } catch (error) {
            console.error('Error fetching about me data:', error);
        }
    };

    fetchAboutMe();

    } else {
      console.log('No token found in local storage');
    }
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleImageChange = (e) => {
    setFormData({ ...formData, profile_image: e.target.files[0] });
  };
  
const handleSubmit = async (e) => {
    e.preventDefault();
    const formDataToSend = new FormData();
    formDataToSend.append('user_id', user_Id);
    formDataToSend.append('name', formData.name);
    formDataToSend.append('phone', formData.phone);
    formDataToSend.append('city', formData.city);
    formDataToSend.append('age', formData.age);
    formDataToSend.append('degree', formData.degree);
    formDataToSend.append('html', formData.html);
    formDataToSend.append('js', formData.js);
    formDataToSend.append('css', formData.css);
    formDataToSend.append('freelance', formData.freelance);
    formDataToSend.append('profile_image', formData.profile_image);
    try {
      const response = await axios.post('http://localhost:5000/api/aboutme', formDataToSend, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
        
      });
  
      setFormData({
        name:'',
        phone: '',
        city: '',
        age: '',
        degree: '',
        bio: '',
        html: '',
        js: '',
        css: '',
        freelance: 'available',
        profile_image: null
      });
      toast.success('About Me updated successfully');
      window.location.reload();
    } catch (error) {
      console.error('Error submitting form:', error);
      toast.error('Failed to update About Me');
    }
  };
  
const aboutUsContent = aboutme !== null && Object.keys(aboutme).length > 0;
  return (
        <div className='container' style={{ width: '75%' }}>
             <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
            {
                aboutUsContent?
                    (
                        <>
                            <h1 className='text-center text-secondary text-uppercase mt-2 fw-bold'>About Me</h1>
                            {/* Bio */}
                            <p className='text-center fs-4 d-block'>"Have no fear of perfection -- you’ll never reach it."</p>
                            <div className="row mt-3 pb-5">
                                <div className="col-md-4">
                                    <div className='h-100'>
                                        <img style={{ height: '100%', objectFit: 'cover' }} className='w-100' src={`http://localhost:5000/uploads/${encodeURIComponent(aboutme.profile_image)}`} alt="Profile" />
                                    </div>
                                </div>
                                <div className="col-md-8">
                                    <div>
                                        <h5 className='pt-3'>Name: {aboutme.name}</h5>
                                        <h5 className='pt-3'>Phone: +975 {aboutme.phone}</h5>
                                        <h5 className='pt-3'>City: {aboutme.city}</h5>
                                        <h5 className='pt-3'>Age: {aboutme.age}</h5>
                                        <h5 className='pt-3'>Degree: {aboutme.degree}</h5>
                                        <h5 className='pt-3'>Freelance: <span className='text-success fw-bold'>{aboutme.freelance}</span></h5>
                                        <div className='mt-4 d-flex flex-column gap-3' style={{ width: '70%' }}>
                                            <div>
                                                <span className='text-uppercase position-relative w-100 d-block pe-5'>
                                                    HTMl
                                                    <span className="position-absolute d-block bottom-2 start-100 translate-middle badge text-dark">
                                                        {aboutme && aboutme.html}%
                                                    </span>
                                                </span>
                                                <div style={{ height: '10px' }} className="progress mt-2" role="progressbar" aria-label="Info example" aria-valuenow={aboutme && aboutme.html} aria-valuemin="0" aria-valuemax="100">
                                                    <div className="progress-bar bg-info" style={{ width: `${aboutme && aboutme.html}%`, height: '10px' }}></div>
                                                </div>
                                            </div>
                                            <div>
                                                <span className='text-uppercase position-relative w-100 d-block pe-5'>
                                                    CSS
                                                    <span className="position-absolute d-block bottom-2 start-100 translate-middle badge text-dark">
                                                        {aboutme && aboutme.css}%
                                                    </span>
                                                </span>
                                                <div style={{ height: '10px' }} className="progress mt-2" role="progressbar" aria-label="Info example" aria-valuenow={aboutme && aboutme.css} aria-valuemin="0" aria-valuemax="100">
                                                    <div className="progress-bar bg-info" style={{ width: `${aboutme && aboutme.css}%`, height: '10px' }}></div>
                                                </div>
                                            </div>
                                            <div>
                                                <span className='text-uppercase position-relative w-100 d-block pe-5'>
                                                    JAVASCRIPTS
                                                    <span className="position-absolute d-block bottom-2 start-100 translate-middle badge text-dark">
                                                        {aboutme && aboutme.js}%
                                                    </span>
                                                </span>
                                                <div style={{ height: '10px' }} className="progress mt-2" role="progressbar" aria-label="Info example" aria-valuenow={aboutme && aboutme.js} aria-valuemin="0" aria-valuemax="100">
                                                    <div className="progress-bar bg-info" style={{ width: `${aboutme && aboutme.js}%`, height: '10px' }}></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='w-100 d-flex justify-content-center align-items-center'>
                                <button data-bs-toggle="modal" data-bs-target="#aboutUsAdd" className='btn btn-secondary px-4 rounded-5 my-3'>Edit About Me</button>
                            </div>

                            {/* Modal */}
                            <div className="modal fade" id="aboutUsAdd" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="aboutUsAddLabel" aria-hidden="true">
                                <div className="modal-dialog  modal-dialog-centered modal-lg">
                                <div className="modal-content p-3" style={{boxShadow: 'rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px'}}>
                                    <div className="modal-header">
                                    <h1 className="modal-title fs-5" id="aboutUsAddLabel">Add About me</h1>
                                    <button  type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    {/* Profile edit form */}
                                    <form  onSubmit={handleSubmit}>
                                    <div className="modal-body">
                                        <div class="mb-3">
                                            <label class="form-label">Profile Image</label>
                                            <input class="form-control" name='profile_image' type="file" onChange={handleImageChange}/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Name</label>
                                            <input  class="form-control" name='name' type="text" onChange={handleChange}/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Phone Number</label>
                                            <input class="form-control" name='phone' type="text" onChange={handleChange}/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">City</label>
                                            <input  class="form-control" name='city' type="text" onChange={handleChange}/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Age</label>
                                            <input  class="form-control" name='age' type="text" onChange={handleChange}/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Degree</label>
                                            <input  class="form-control" name='degree' type="text" onChange={handleChange}/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Skills (Number in percent)</label>
                                            <div className='col-12 d-flex gap-3'>
                                                <input className='form-control' name='html' placeholder='HMTL' type="text" onChange={handleChange}/>
                                                <input className='form-control' name='css' placeholder='JS' type="text" onChange={handleChange}/>
                                                <input className='form-control' name='js' placeholder='CSS' type="text" onChange={handleChange}/>
                                            </div>
                                        </div>

                                        <div className='mb-3'>
                                            <label className='form-label'>Availability</label>
                                            <select className='form-select' name="freelance" onChange={handleChange}>
                                                <option value='available'>Available</option>
                                                <option value='Not Available'>Not Available</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="modal-footer mb-3">
                                        <button type="button" className="btn btn-primary rounded-5" data-bs-dismiss="modal">Close</button>
                                        <button type="submit" className="btn btn-secondary rounded-5 px-4">Save the Changes</button>
                                    </div>
                                    </form>
                                </div>
                                </div>
                            </div>
                        </>
                    )
                    :
                    (
                        <>
                            <h1 className='text-center text-secondary text-uppercase mt-2 fw-bold'>About Me</h1>
                            <div className='w-100 d-flex justify-content-center align-items-center'>
                                <button data-bs-toggle="modal" data-bs-target="#aboutUsAdd" className='btn btn-secondary px-4 rounded-5 my-3'>Add About Me</button>
                            </div>
                            {/* Add Modal */}
                            <div className="modal fade" id="aboutUsAdd" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="aboutUsAddLabel" aria-hidden="true">
                                <div className="modal-dialog  modal-dialog-centered modal-lg">
                                <div className="modal-content p-3" style={{boxShadow: 'rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px'}}>
                                    <div className="modal-header">
                                    <h1 className="modal-title fs-5" id="aboutUsAddLabel">Add About me</h1>
                                    <button  type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    {/* Profile edit form */}
                                    <form  onSubmit={handleSubmit}>
                                    <div className="modal-body">
                                        <div class="mb-3">
                                            <label class="form-label">Profile Image</label>
                                            <input class="form-control" name='profile_image' type="file" onChange={handleImageChange}/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Name</label>
                                            <input  class="form-control" name='name' type="text" onChange={handleChange}/>
                                                </div>
                                        <div class="mb-3">
                                            <label class="form-label">Phone Number</label>
                                            <input class="form-control" name='phone' type="text" onChange={handleChange}/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">City</label>
                                            <input  class="form-control" name='city' type="text" onChange={handleChange}/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Age</label>
                                            <input  class="form-control" name='age' type="text" onChange={handleChange}/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Degree</label>
                                            <input  class="form-control" name='degree' type="text" onChange={handleChange}/>
                                        </div>
                                        <div class="mb-3">
                                            <label class="form-label">Skills (Number in percent)</label>
                                            <div className='col-12 d-flex gap-3'>
                                                <input className='form-control' name='html' placeholder='HMTL' type="text" onChange={handleChange}/>
                                                <input className='form-control' name='css' placeholder='JS' type="text" onChange={handleChange}/>
                                                <input className='form-control' name='js' placeholder='CSS' type="text" onChange={handleChange}/>
                                            </div>
                                        </div>

                                        <div className='mb-3'>
                                            <label className='form-label'>Availability</label>
                                            <select className='form-select' name="freelance" onChange={handleChange}>
                                                <option value='available'>Available</option>
                                                <option value='Not Available'>Not Available</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="modal-footer mb-3">
                                        <button type="button" className="btn btn-primary rounded-5" data-bs-dismiss="modal">Close</button>
                                        <button type="submit" className="btn btn-secondary rounded-5 px-4">Save the Changes</button>
                                    </div>
                                    </form>
                                </div>
                                </div>
                            </div>
                        </>
                    )
            }
        </div>
  )
}

export default About