import React, { useState, useEffect } from 'react';
import axios from "axios";
import { jwtDecode } from 'jwt-decode';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Resume = () => {
    const [user_Id, setUser_Id] = useState('');
    const [resume, setResume] = useState([]);

    const [formData, setFormData] = useState({
        summary: '',
        experience: '',
        education: '',
    });

    useEffect(() => {
        const token = localStorage.getItem('jwtToken');
        if (token) {
          const decodedToken = jwtDecode(token);
          const userId = decodedToken.user.id;
          setUser_Id(userId);
    
          // fetch data
          const fetchResume = async () => {
            try {
                const response = await axios.get(`http://localhost:5000/api/getResume/${userId}`);
                setResume(response.data);
            } catch (error) {
                console.error('Error fetching about me data:', error);
            }
        };
    
        fetchResume();
    
        } else {
          console.log('No token found in local storage');
        }
      }, []);
    
      const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
      };
      
    const handleSubmit = async (e) => {
        e.preventDefault();
        const formDataToSend = new FormData();
        formDataToSend.append('user_id', user_Id);
        formDataToSend.append('summary', formData.summary);
        formDataToSend.append('experience', formData.experience);
        formDataToSend.append('education', formData.education);

        try {
          const response = await axios.post('http://localhost:5000/api/resume', formDataToSend, {
            headers: {
                'Content-Type': 'multipart/form-data'
              }      
          });
      
          setFormData({
            summary: '',
            experience: '',
            education: '',
          });
          toast.success('Resume updated successfully');
          window.location.reload();
        } catch (error) {
          console.error('Error submitting form:', error);
          toast.error('Failed to update Resume');
        }
      };

      const resumeContent = resume !== null && Object.keys(resume).length > 0;
  return (
    <div className='container' style={{width:'75%'}}>
        <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
        <h1 className='text-center text-secondary text-uppercase mt-2 fw-bold'>resume</h1>
        {
                resumeContent?
                    (
            <>
                <i className='text-center fs-4 d-block'>"Have no fear of perfection -- you’ll never reach it."</i>
                <div className="row mt-3">
                    <div className="col-md-6">
                        <h3 className='fw-bold text-center'>Summary</h3>
                        <p>
                            {resume.summary}
                        </p>
                    </div>
                    <div className="col-md-6">
                    <h3 className='fw-bold'>Professional Experience</h3>
                        <p>
                            {resume.experience}
                        </p>
                    </div>
                    <div className="col-md-6">
                        <h3 className='fw-bold text-center mt-3'>Education</h3>
                        <p>
                            {resume.education}
                        </p>
                    </div>
                </div>
                <div className='w-100 d-flex justify-content-center align-items-center'>
                    <button data-bs-toggle="modal" data-bs-target="#resumeAdd" className='btn btn-secondary px-4 rounded-5 my-3'>Edit Resume</button>
                </div>

                {/* Modal */}
                <div className="modal fade" id="resumeAdd" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="resumeAddLabel" aria-hidden="true">
                    <div className="modal-dialog  modal-dialog-centered modal-lg">
                    <div className="modal-content p-3" style={{boxShadow: 'rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px'}}>
                        <div className="modal-header">
                        <h1 className="modal-title fs-5" id="resumeAddLabel">Add Resume</h1>
                        <button  type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        {/* Resume form */}
                        <form onSubmit={handleSubmit}>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <div>
                                        <div className='mb-3'>
                                            <h4>Summary</h4>
                                            <textarea rows="5" className='form-control' name='summary'  onChange={handleChange}/>
                                        </div>
                                        <div>
                                            <h4>Education</h4>
                                            <textarea className='form-control' rows="5" name='education'  onChange={handleChange}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className='h-100'>
                                        <h4>Profession Experience</h4>
                                        <textarea className='form-control' rows="13" name='experience' onChange={handleChange}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer mb-3">
                            <button type="button" className="btn btn-primary rounded-5" data-bs-dismiss="modal">Close</button>
                            <button type="submit" className="btn btn-secondary rounded-5 px-4">Add Resume</button>
                        </div>
                        </form>
                    </div>
                    </div>
                </div>
            </>
            )
            :
            (
            <>
            <div className='w-100 d-flex justify-content-center align-items-center'>
                <button data-bs-toggle="modal" data-bs-target="#resumeAdd" className='btn btn-secondary px-4 rounded-5 my-3'>Add Resume</button>
            </div>
            <div className="modal fade" id="resumeAdd" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="resumeAddLabel" aria-hidden="true">
                <div className="modal-dialog  modal-dialog-centered modal-lg">
                <div className="modal-content p-3" style={{boxShadow: 'rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px'}}>
                    <div className="modal-header">
                    <h1 className="modal-title fs-5" id="resumeAddLabel">Add Resume</h1>
                    <button  type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    {/* Resume form */}
                    <form onSubmit={handleSubmit}>
                    <div className="modal-body">
                        <div className="row">
                            <div className="col-md-6">
                                <div>
                                    <div className='mb-3'>
                                        <h4>Summary</h4>
                                        <textarea rows="5" className='form-control' name='summary'  onChange={handleChange}/>
                                    </div>
                                    <div>
                                        <h4>Education</h4>
                                        <textarea className='form-control' rows="5" name='education'  onChange={handleChange}/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className='h-100'>
                                    <h4>Profession Experience</h4>
                                    <textarea className='form-control' rows="13" name='experience' onChange={handleChange}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer mb-3">
                        <button type="button" className="btn btn-primary rounded-5" data-bs-dismiss="modal">Close</button>
                        <button type="submit" className="btn btn-secondary rounded-5 px-4">Add Resume</button>
                    </div>
                    </form>
                </div>
                </div>
            </div>
            </>
            )
        }

    </div>
  )
}

export default Resume