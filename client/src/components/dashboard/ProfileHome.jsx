import React, { useState, useEffect } from 'react';
import axios from "axios";
import { jwtDecode } from 'jwt-decode';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ProfileHome = () => {
  const [user_Id, setUser_Id] = useState('');
  const [profile, setProfile] = useState([]);

  const [formData, setFormData] = useState({
    name: '',
    profession: '',
    profile_bg: null
  });

  useEffect(() => {
    const token = localStorage.getItem('jwtToken');
    if (token) {
      const decodedToken = jwtDecode(token);
      const userId = decodedToken.user.id;
      setUser_Id(userId);

      // fetch data
      const fetchProfile = async () => {
        try {
            const response = await axios.get(`http://localhost:5000/api/getProfile/${userId}`);
            setProfile(response.data);
        } catch (error) {
            console.error('Error fetching profile data:', error);
        }
    };

    fetchProfile();

    } else {
      console.log('No token found in local storage');
    }
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleImageChange = (e) => {
    setFormData({ ...formData, profile_bg: e.target.files[0] });
  };
  
const handleSubmit = async (e) => {
    e.preventDefault();
    const formDataToSend = new FormData();
    formDataToSend.append('user_id', user_Id);
    formDataToSend.append('name', formData.name);
    formDataToSend.append('profession', formData.profession);
    formDataToSend.append('profile_bg', formData.profile_bg);

    try {
      const response = await axios.post('http://localhost:5000/api/profile', formDataToSend, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
  
      setFormData({
        name: '',
        profession: '',
        profile_bg: null
      });
      toast.success('Profile updated successfully');
      window.location.reload();
    } catch (error) {
      console.error('Error submitting form:', error);
      toast.error('Failed to update Profile');
    }
  };

  console.log(profile);
  return (
    <div className='h-100 position-relative'>
       <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
      {/* backgroun Image */}
      {profile.profile_bg ? (
          <img
            style={{ height: '100%', objectFit: 'cover' }}
            className='w-100'
            src={`http://localhost:5000/uploads/${encodeURIComponent(profile.profile_bg)}`}
            alt="Profile Background"
          />
        ) : (
          <img
            style={{ height: '90vh', objectFit: 'cover', objectPosition: 'top' }}
            className='w-100'
            src="https://images.rawpixel.com/image_800/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvcm00MjgtMDAwMi5qcGc.jpg"
            alt="Default Background"
          />
        )}
       {/* Name and profession */}
       <div style={{ position: 'absolute', top: '50%', right: '40%', transform: 'translateY(-50%)', textAlign: 'center' }}>
          <h1 className='fw-bold'>{profile.name}</h1>
          <p className='fw-bold'>{profile.profession}</p>
          <div className='w-100 d-flex justify-content-center align-items-center'>
            <button data-bs-toggle="modal" data-bs-target="#profileEdit" style={{width:'40px',height:'40px'}} className='btn btn-info rounded-circle border-2 border-white d-flex justify-content-center align-items-center'>
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style={{fill: '#fff'}}><path d="M8.707 19.707 18 10.414 13.586 6l-9.293 9.293a1.003 1.003 0 0 0-.263.464L3 21l5.242-1.03c.176-.044.337-.135.465-.263zM21 7.414a2 2 0 0 0 0-2.828L19.414 3a2 2 0 0 0-2.828 0L15 4.586 19.414 9 21 7.414z"></path></svg>
            </button>
          </div>
        </div>
        {/* Modal Profile edit */}
      <div className="modal fade" id="profileEdit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="profileEditLabel" aria-hidden="true">
        <div className="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered">
          <div className="modal-content p-3" style={{boxShadow: 'rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px'}}>
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="profileEditLabel">Profile Edit</h1>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            {/* Profile edit form */}
            <form onSubmit={handleSubmit}>
              <div className="modal-body">
                <div class="mb-3">
                  <label class="form-label">Background Image</label>
                  <input class="form-control" type="file" name='profile_bg' onChange={handleImageChange}/>
                </div>
                <div class="mb-3">
                  <label class="form-label">Name</label>
                  <input class="form-control" type="text" name='name' onChange={handleChange}/>
                </div>
                <div class="mb-3">
                  <label class="form-label">Your Profession</label>
                  {/* <input value="UX & UI" class="form-control" type="text" name='profession' onChange={handleChange}/> */}
                  <input class="form-control" type="text" name='profession' onChange={handleChange}/>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary rounded-5" data-bs-dismiss="modal">Close</button>
                <button type="submit" className="btn btn-secondary rounded-5 px-4">Save the Changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProfileHome