import React from 'react'
import { NavLink } from 'react-router-dom'

const Contact = () => {
  return (
    <div className='container' style={{width:'75%'}}>
        <h1 className='text-center text-secondary text-uppercase mt-2 fw-bold'>Contact Me</h1>
        {/* Bio */}
        <i className='text-center fs-4 d-block'>"Have no fear of perfection -- you’ll never reach it."</i>
        <div className="row g-2 mt-3 pb-5">
            <div className="col-md-12">
                <div className='h-100'>
                    <div className="row g-2">
                        <div className="col-12">
                            <div className='p-5 d-flex flex-column gap-3 align-items-center justify-content-center' style={{background:'#D9D9D9'}}>
                                 <h5 className='fw-bold'>Social Profile</h5>
                                 <div className='d-flex gap-2 flex-wrap'>
                                     <NavLink style={{width:'50px',height:'50px'}} className='bg-dark rounded-circle d-flex justify-content-center align-items-center'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style={{fill: '#fff'}}><path d="M19.633 7.997c.013.175.013.349.013.523 0 5.325-4.053 11.461-11.46 11.461-2.282 0-4.402-.661-6.186-1.809.324.037.636.05.973.05a8.07 8.07 0 0 0 5.001-1.721 4.036 4.036 0 0 1-3.767-2.793c.249.037.499.062.761.062.361 0 .724-.05 1.061-.137a4.027 4.027 0 0 1-3.23-3.953v-.05c.537.299 1.16.486 1.82.511a4.022 4.022 0 0 1-1.796-3.354c0-.748.199-1.434.548-2.032a11.457 11.457 0 0 0 8.306 4.215c-.062-.3-.1-.611-.1-.923a4.026 4.026 0 0 1 4.028-4.028c1.16 0 2.207.486 2.943 1.272a7.957 7.957 0 0 0 2.556-.973 4.02 4.02 0 0 1-1.771 2.22 8.073 8.073 0 0 0 2.319-.624 8.645 8.645 0 0 1-2.019 2.083z"></path></svg>
                                     </NavLink>
                                     <NavLink style={{width:'50px',height:'50px'}} className='bg-dark rounded-circle d-flex justify-content-center align-items-center'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style={{fill: '#fff'}}><path d="M13.397 20.997v-8.196h2.765l.411-3.209h-3.176V7.548c0-.926.258-1.56 1.587-1.56h1.684V3.127A22.336 22.336 0 0 0 14.201 3c-2.444 0-4.122 1.492-4.122 4.231v2.355H7.332v3.209h2.753v8.202h3.312z"></path></svg>                                     </NavLink>
                                     <NavLink style={{width:'50px',height:'50px'}} className='bg-dark rounded-circle d-flex justify-content-center align-items-center'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style={{fill: '#fff'}}><circle cx="4.983" cy="5.009" r="2.188"></circle><path d="M9.237 8.855v12.139h3.769v-6.003c0-1.584.298-3.118 2.262-3.118 1.937 0 1.961 1.811 1.961 3.218v5.904H21v-6.657c0-3.27-.704-5.783-4.526-5.783-1.835 0-3.065 1.007-3.568 1.96h-.051v-1.66H9.237zm-6.142 0H6.87v12.139H3.095z"></path></svg>                                     
                                     </NavLink>
                                 </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Contact