import React, { useState, useEffect } from 'react';
import axios from "axios";
import { jwtDecode } from 'jwt-decode';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Portfolio = () => {
  const [user_Id, setUser_Id] = useState('');
  const [portfolio, setPortfolio] = useState([]);

  const [formData, setFormData] = useState({
    name: '',
    email: '',
    description: '',
    upload: null
  });

  useEffect(() => {
    const token = localStorage.getItem('jwtToken');
    if (token) {
      const decodedToken = jwtDecode(token);
      const userId = decodedToken.user.id;
      setUser_Id(userId);

      // fetch data
      const fetchPortfolio = async () => {
        try {
            const response = await axios.get(`http://localhost:5000/api/getPortfolio/${userId}`);
            setPortfolio(response.data);
        } catch (error) {
            console.error('Error fetching portfolio data:', error);
        }
    };

    fetchPortfolio();
    
        } else {
          console.log('No token found in local storage');
        }
      }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleImageChange = (e) => {
    setFormData({ ...formData, upload: e.target.files[0] });
  };
  
  const handleSubmit = async (e) => {
    e.preventDefault();
    const formDataToSend = new FormData();
    formDataToSend.append('user_id', user_Id);
    formDataToSend.append('name', formData.name);
    formDataToSend.append('email', formData.email);
    formDataToSend.append('description', formData.description);
    formDataToSend.append('upload', formData.upload);

    try {
      const response = await axios.post('http://localhost:5000/api/portfolio', formDataToSend, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });

      console.log(response);
  
      setFormData({
        name: '',
        email: '',
        description: '',
        upload: null
      });
      toast.success('Portfolio updated successfully');
      window.location.reload();
    } catch (error) {
      console.error('Error submitting form:', error);
      toast.error('Failed to update Portfolio');
    }
  };

  return (
    <div style={{width:'75%'}} className='container'>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
        <h1 className='text-center text-secondary text-uppercase mt-2 fw-bold'>my portfolio</h1>
          {portfolio.length > 0 ? (
            <div className="row">
              {portfolio.map((item, index) => (
                <div className="col-lg-4 col-md-12 mb-4 mb-lg-0" key={index}>
                <img
                  src={`http://localhost:5000/uploads/${encodeURIComponent(item.upload)}`}
                  alt="Portfolio"
                  className="w-100 h-auto shadow-1-strong rounded mb-4"
                  style={{ maxHeight: "200px" }}
                />
              </div>
              
              
              ))}
            </div>
          ):(<div class="row">
          <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">
              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(73).webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Boat on Calm Water"
              />

              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Vertical/mountain1.webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Wintry Mountain Landscape"
              />
          </div>

          <div class="col-lg-4 mb-4 mb-lg-0">
              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Vertical/mountain2.webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Mountains in the Clouds"
              />

              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(73).webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Boat on Calm Water"
              />
          </div>

          <div class="col-lg-4 mb-4 mb-lg-0">
              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(18).webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Waves at Sea"
              />

              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Vertical/mountain3.webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Yosemite National Park"
              />
          </div>
          </div>)}

        <div className='w-100 d-flex justify-content-center align-items-center'>
            <button data-bs-toggle="modal" data-bs-target="#uploadPortfolio" className='btn btn-secondary fs-5 fw-bold py-3 px-5 rounded-5 my-3'>Upload Portfolio</button>
        </div>
        {/* upload portfolio */}
        <div className="modal fade" id="uploadPortfolio" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="uploadPortfolioLabel" aria-hidden="true">
                <div className="modal-dialog  modal-dialog-centered modal-lg">
                <div className="modal-content p-3" style={{boxShadow: 'rgba(0, 0, 0, 0.25) 0px 54px 55px, rgba(0, 0, 0, 0.12) 0px -12px 30px, rgba(0, 0, 0, 0.12) 0px 4px 6px, rgba(0, 0, 0, 0.17) 0px 12px 13px, rgba(0, 0, 0, 0.09) 0px -3px 5px'}}>
                    <div className="modal-header">
                    <h1 className="modal-title fs-2 fw-bold text-center text-uppercase" id="uploadPortfolioLabel">Upload form</h1>
                    <button  type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    {/* Resume form */}
                    <form onSubmit={handleSubmit}>
                    <div className="modal-body">
                        <div className="row">
                            <div className="col-md-6">
                                <div className='h-100'>
                                    <div className='mb-3'>
                                        <h5 className='text-uppercase'>Name</h5>
                                        <input className='form-control bg-secondary text-white' name='name' onChange={handleChange}/>
                                    </div>
                                    <div className='mb-3'>
                                        <h5 className='text-uppercase'>Email</h5>
                                        <input className='form-control bg-secondary text-white' name='email' onChange={handleChange}/>
                                    </div>
                                    <div>
                                        <h5 className='text-uppercase'>Description</h5>
                                        <textarea className='form-control bg-secondary text-white' rows="10" name='description' onChange={handleChange}/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6" style={{ height: '400px' }}>
                                {formData.upload ? (
                                    <img src={URL.createObjectURL(formData.upload)} alt="Uploaded" style={{ maxWidth: '100%', maxHeight: '100%', marginTop: '20px' }} />
                                ) : (
                                <div className='h-100 bg-secondary rounded'>
                                    <h5 className='text-uppercase bg-white pb-2'>Upload your project</h5>
                                    <div className='d-flex justify-content-center align-items-center h-75'>
                                        <label class="custum-file-upload" for="file">
                                          <div class="icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="" viewBox="0 0 24 24"><g stroke-width="0" id="SVGRepo_bgCarrier"></g><g stroke-linejoin="round" stroke-linecap="round" id="SVGRepo_tracerCarrier"></g><g id="SVGRepo_iconCarrier"> <path fill="" d="M10 1C9.73478 1 9.48043 1.10536 9.29289 1.29289L3.29289 7.29289C3.10536 7.48043 3 7.73478 3 8V20C3 21.6569 4.34315 23 6 23H7C7.55228 23 8 22.5523 8 22C8 21.4477 7.55228 21 7 21H6C5.44772 21 5 20.5523 5 20V9H10C10.5523 9 11 8.55228 11 8V3H18C18.5523 3 19 3.44772 19 4V9C19 9.55228 19.4477 10 20 10C20.5523 10 21 9.55228 21 9V4C21 2.34315 19.6569 1 18 1H10ZM9 7H6.41421L9 4.41421V7ZM14 15.5C14 14.1193 15.1193 13 16.5 13C17.8807 13 19 14.1193 19 15.5V16V17H20C21.1046 17 22 17.8954 22 19C22 20.1046 21.1046 21 20 21H13C11.8954 21 11 20.1046 11 19C11 17.8954 11.8954 17 13 17H14V16V15.5ZM16.5 11C14.142 11 12.2076 12.8136 12.0156 15.122C10.2825 15.5606 9 17.1305 9 19C9 21.2091 10.7909 23 13 23H20C22.2091 23 24 21.2091 24 19C24 17.1305 22.7175 15.5606 20.9844 15.122C20.7924 12.8136 18.858 11 16.5 11Z" clip-rule="evenodd" fill-rule="evenodd"></path> </g></svg>
                                          </div>
                                          <div class="text">
                                            <span className='text-white'>Click to upload image</span>
                                          </div>
                                      
                                             <input type="file" id="file" name='upload' onChange={handleImageChange} />
                                        </label>
                                    </div>
                                </div>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="d-flex justify-content-center mb-3">
                        <button type="submit" className="btn btn-secondary rounded-5 px-5">Upload</button>
                    </div>
                    </form>
                </div>
                </div>
        </div>
    </div>
  )
}

export default Portfolio