import React, { useState, useEffect } from 'react';
import axios from "axios";
import { useLocation } from 'react-router-dom';

const ProfileHome = () => {
  const [profile, setProfile] = useState([]);

  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const userId = searchParams.get('user_id');

  const fetchProfile = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/api/getProfile/${userId}`);
        setProfile(response.data);
      } catch (error) {
        console.error('Error fetching project data:', error);
      }
    };
  
    useEffect(() => {
      fetchProfile();
    }, []);
  return (
    <div className='h-100 position-relative'>
      {/* backgroun Image */}
      {profile.profile_bg ? (
          <img
            style={{ height: '100%', objectFit: 'cover' }}
            className='w-100'
            src={`http://localhost:5000/uploads/${encodeURIComponent(profile.profile_bg)}`}
            alt="Profile Background"
          />
        ) : (
          <img
            style={{ height: '90vh', objectFit: 'cover', objectPosition: 'top' }}
            className='w-100'
            src="https://images.rawpixel.com/image_800/czNmcy1wcml2YXRlL3Jhd3BpeGVsX2ltYWdlcy93ZWJzaXRlX2NvbnRlbnQvbHIvcm00MjgtMDAwMi5qcGc.jpg"
            alt="Default Background"
          />
        )}
       {/* Name and profession */}
       <div style={{ position: 'absolute', top: '50%', right: '40%', transform: 'translateY(-50%)', textAlign: 'center' }}>
          <h1 className='fw-bold'>{profile.name}</h1>
          <p className='fw-bold'>{profile.profession}</p>
        </div>
    </div>
  )
}

export default ProfileHome