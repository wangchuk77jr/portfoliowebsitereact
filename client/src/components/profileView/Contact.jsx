import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { NavLink } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Contact = () => {
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const userId = searchParams.get('user_id');

    const [email, setEmail] = useState(null);
    useEffect(() => {
        if (userId) {
            axios.get(`http://localhost:5000/api/getMail/${userId}`)
                .then(response => {
                    const userData = response.data;
                    setEmail(userData.email);
                })
                .catch(error => {
                    console.error('Error fetching user data:', error);
                });
        }
    }, [userId]);

    const [formData, setFormData] = useState({
        name: '',
        sender: '',
        subject: '',
        message: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
      };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const formDataToSend = {
        email,
        name: formData.name,
        sender: formData.sender,
        subject: formData.subject,
        message: formData.message
        };
    
        try {
        const response = await axios.post('http://localhost:5000/api/sendEmail', formDataToSend, {
            headers: {
            'Content-Type': 'application/json'
            }
        });
    
        console.log(response.data);
    
        setFormData({
            name: '',
            sender: '',
            subject: '',
            message: ''
        });
        toast.success('Send mail updated successfully');
        window.location.reload();
        } catch (error) {
        console.error('Error submitting form:', error);
        toast.error('Failed to update mail');
        }
    };
  
  return (
    <div className='container' style={{width:'75%'}}>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
        <h1 className='text-center text-secondary text-uppercase mt-2 fw-bold'>Contact Me</h1>
        {/* Bio */}
        <i className='text-center fs-4 d-block'>"Have no fear of perfection -- you’ll never reach it."</i>
        <div className="row g-2 mt-3 pb-5">
            <div className="col-md-6">
                <div className='h-100'>
                    <div className="row g-2">
                        <div className="col-12">
                            <div className='p-5 d-flex flex-column gap-3 align-items-center justify-content-center' style={{background:'#D9D9D9'}}>
                                 <h5 className='fw-bold'>Social Profile</h5>
                                 <div className='d-flex gap-2 flex-wrap'>
                                     <NavLink style={{width:'50px',height:'50px'}} className='bg-dark rounded-circle d-flex justify-content-center align-items-center'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style={{fill: '#fff'}}><path d="M19.633 7.997c.013.175.013.349.013.523 0 5.325-4.053 11.461-11.46 11.461-2.282 0-4.402-.661-6.186-1.809.324.037.636.05.973.05a8.07 8.07 0 0 0 5.001-1.721 4.036 4.036 0 0 1-3.767-2.793c.249.037.499.062.761.062.361 0 .724-.05 1.061-.137a4.027 4.027 0 0 1-3.23-3.953v-.05c.537.299 1.16.486 1.82.511a4.022 4.022 0 0 1-1.796-3.354c0-.748.199-1.434.548-2.032a11.457 11.457 0 0 0 8.306 4.215c-.062-.3-.1-.611-.1-.923a4.026 4.026 0 0 1 4.028-4.028c1.16 0 2.207.486 2.943 1.272a7.957 7.957 0 0 0 2.556-.973 4.02 4.02 0 0 1-1.771 2.22 8.073 8.073 0 0 0 2.319-.624 8.645 8.645 0 0 1-2.019 2.083z"></path></svg>
                                     </NavLink>
                                     <NavLink style={{width:'50px',height:'50px'}} className='bg-dark rounded-circle d-flex justify-content-center align-items-center'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style={{fill: '#fff'}}><path d="M13.397 20.997v-8.196h2.765l.411-3.209h-3.176V7.548c0-.926.258-1.56 1.587-1.56h1.684V3.127A22.336 22.336 0 0 0 14.201 3c-2.444 0-4.122 1.492-4.122 4.231v2.355H7.332v3.209h2.753v8.202h3.312z"></path></svg>                                     </NavLink>
                                     <NavLink style={{width:'50px',height:'50px'}} className='bg-dark rounded-circle d-flex justify-content-center align-items-center'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style={{fill: '#fff'}}><circle cx="4.983" cy="5.009" r="2.188"></circle><path d="M9.237 8.855v12.139h3.769v-6.003c0-1.584.298-3.118 2.262-3.118 1.937 0 1.961 1.811 1.961 3.218v5.904H21v-6.657c0-3.27-.704-5.783-4.526-5.783-1.835 0-3.065 1.007-3.568 1.96h-.051v-1.66H9.237zm-6.142 0H6.87v12.139H3.095z"></path></svg>                                     
                                     </NavLink>
                                 </div> 
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className=' pt-2 d-flex flex-column gap-1 align-items-center justify-content-center' style={{background:'#D9D9D9'}}>
                                <div className='d-flex gap-2 flex-wrap'>
                                     <NavLink style={{width:'50px',height:'50px'}} className='bg-dark rounded-circle d-flex justify-content-center align-items-center'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style={{fill: '#fff'}}><path d="M20 4H4c-1.103 0-2 .897-2 2v12c0 1.103.897 2 2 2h16c1.103 0 2-.897 2-2V6c0-1.103-.897-2-2-2zm0 2v.511l-8 6.223-8-6.222V6h16zM4 18V9.044l7.386 5.745a.994.994 0 0 0 1.228 0L20 9.044 20.002 18H4z"></path></svg>
                                    </NavLink>
                                 </div>
                                 <h5 className='fw-bold'>Email Me</h5>
                                 <p>12200001.gcit@rub.edu.bt</p>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className=' pt-2 d-flex flex-column gap-1 align-items-center justify-content-center' style={{background:'#D9D9D9'}}>
                                <div className='d-flex gap-2 flex-wrap'>
                                     <NavLink style={{width:'50px',height:'50px'}} className='bg-dark rounded-circle d-flex justify-content-center align-items-center'>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style={{fill: '#fff'}}><path d="M20 10.999h2C22 5.869 18.127 2 12.99 2v2C17.052 4 20 6.943 20 10.999z"></path><path d="M13 8c2.103 0 3 .897 3 3h2c0-3.225-1.775-5-5-5v2zm3.422 5.443a1.001 1.001 0 0 0-1.391.043l-2.393 2.461c-.576-.11-1.734-.471-2.926-1.66-1.192-1.193-1.553-2.354-1.66-2.926l2.459-2.394a1 1 0 0 0 .043-1.391L6.859 3.513a1 1 0 0 0-1.391-.087l-2.17 1.861a1 1 0 0 0-.29.649c-.015.25-.301 6.172 4.291 10.766C11.305 20.707 16.323 21 17.705 21c.202 0 .326-.006.359-.008a.992.992 0 0 0 .648-.291l1.86-2.171a1 1 0 0 0-.086-1.391l-4.064-3.696z"></path></svg>
                                     </NavLink>
                                 </div>
                                 <h5 className='fw-bold'>Call Me</h5>
                                 <p>+975 7719821</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-6">
                <div style={{background:'#D9D9D9'}} className='h-100 p-2'>
                <form onSubmit={handleSubmit}>
                    <div className="row g-3 mb-2">
                        <div className="col">
                            <input type="text" name='name' onChange={handleChange} className="form-control rounded-0" placeholder="Your Name"/>
                        </div>
                        <div className="col">
                            <input type="email" name='sender' onChange={handleChange} className="form-control rounded-0" placeholder="Your Email" />
                        </div>
                    </div>
                    <input type="text" name='subject' onChange={handleChange} className="form-control mb-2 rounded-0" placeholder="Subject" />
                    <textarea placeholder='Message' name='message' onChange={handleChange} className='form-control mb-2 rounded-0' rows="7" />
                    <div className='d-flex justify-content-center'>
                        <button type='submit' className='btn btn-info text-white rounded-5 px-4'>Send Message</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Contact