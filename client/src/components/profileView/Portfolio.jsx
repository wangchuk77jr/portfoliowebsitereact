import React, { useState, useEffect } from 'react';
import axios from "axios";
import { useLocation } from 'react-router-dom';

const Portfolio = () => {
    const [portfolio, setPortfolio] = useState([]);

    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const userId = searchParams.get('user_id');

    const fetchPortfolio = async () => {
        try {
          const response = await axios.get(`http://localhost:5000/api/getPortfolio/${userId}`);
          setPortfolio(response.data);
        } catch (error) {
          console.error('Error fetching project data:', error);
        }
      };
    
      useEffect(() => {
        fetchPortfolio();
      }, []);
  return (
    <div style={{width:'75%'}} className='container'>
        <h1 className='text-center text-secondary text-uppercase mt-2 fw-bold'>my portfolio</h1>
        {portfolio.length > 0 ? (
            <div className="row">
              {portfolio.map((item, index) => (
                <div className="col-lg-4 col-md-12 mb-4 mb-lg-0" key={index}>
                <img
                  src={`http://localhost:5000/uploads/${encodeURIComponent(item.upload)}`}
                  alt="Portfolio"
                  className="w-100 h-auto shadow-1-strong rounded mb-4"
                  style={{ maxHeight: "200px" }}
                />
              </div>  
              ))}
            </div>
          ):(<div class="row">
          <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">
              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(73).webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Boat on Calm Water"
              />

              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Vertical/mountain1.webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Wintry Mountain Landscape"
              />
          </div>

          <div class="col-lg-4 mb-4 mb-lg-0">
              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Vertical/mountain2.webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Mountains in the Clouds"
              />

              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(73).webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Boat on Calm Water"
              />
          </div>

          <div class="col-lg-4 mb-4 mb-lg-0">
              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Horizontal/Nature/4-col/img%20(18).webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Waves at Sea"
              />

              <img
              src="https://mdbcdn.b-cdn.net/img/Photos/Vertical/mountain3.webp"
              class="w-100 shadow-1-strong rounded mb-4"
              alt="Yosemite National Park"
              />
          </div>
          </div>)}
    </div>
  )
}

export default Portfolio