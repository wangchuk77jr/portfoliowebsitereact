import React, { useState, useEffect } from 'react';
import axios from "axios";
import { useLocation } from 'react-router-dom';

const Resume = () => {
    const [resume, setResume] = useState([]);

    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const userId = searchParams.get('user_id');

    const fetchResume = async () => {
        try {
          const response = await axios.get(`http://localhost:5000/api/getResume/${userId}`);
          setResume(response.data);
        } catch (error) {
          console.error('Error fetching project data:', error);
        }
      };
    
      useEffect(() => {
        fetchResume();
      }, []);
  return (
    <div className='container' style={{width:'75%'}}>
        <h1 className='text-center text-secondary text-uppercase mt-2 fw-bold'>resume</h1>
        {/* Bio */}
        <i className='text-center fs-4 d-block'>"Have no fear of perfection -- you’ll never reach it."</i>
            <div className="row mt-3">
                    <div className="col-md-6">
                        <h3 className='fw-bold text-center'>Summary</h3>
                        <p>
                            {resume.summary}
                        </p>
                    </div>
                    <div className="col-md-6">
                    <h3 className='fw-bold'>Professional Experience</h3>
                        <p>
                            {resume.experience}
                        </p>
                    </div>
                    <div className="col-md-6">
                        <h3 className='fw-bold text-center mt-3'>Education</h3>
                        <p>
                            {resume.education}
                        </p>
                    </div>
            </div>
    </div>
  )
}

export default Resume