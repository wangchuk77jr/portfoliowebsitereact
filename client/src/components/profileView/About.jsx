import React, { useState, useEffect } from 'react';
import axios from "axios";
import { useLocation } from 'react-router-dom';

const About = () => {
    const [aboutme, setAboutMe] = useState([]);

    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const userId = searchParams.get('user_id');

    const fetchAboutme = async () => {
        try {
          const response = await axios.get(`http://localhost:5000/api/getAboutme/${userId}`);
          setAboutMe(response.data);
        } catch (error) {
          console.error('Error fetching project data:', error);
        }
      };
    
      useEffect(() => {
        fetchAboutme();
      }, []);
    return (
            <div className='container' style={{width:'75%'}}>
                <h1 className='text-center text-secondary text-uppercase mt-2 fw-bold'>About Me</h1>
                    <div className="row mt-3 pb-5">
                        <div className="col-md-4">
                            <div className='h-100'>
                                <img style={{ height: '100%', objectFit: 'cover' }} className='w-100' src={`http://localhost:5000/uploads/${encodeURIComponent(aboutme.profile_image)}`} alt="Profile" />
                            </div>
                        </div>
                        <div className="col-md-8">
                            <div>
                                <h5 className='pt-3'>Name: {aboutme.name}</h5>
                                <h5 className='pt-3'>Phone: +975 {aboutme.phone}</h5>
                                <h5 className='pt-3'>City: {aboutme.city}</h5>
                                <h5 className='pt-3'>Age: {aboutme.age}</h5>
                                <h5 className='pt-3'>Degree: {aboutme.degree}</h5>
                                <h5 className='pt-3'>Freelance: <span className='text-success fw-bold'>{aboutme.freelance}</span></h5>
                                <div className='mt-4 d-flex flex-column gap-3' style={{ width: '70%' }}>
                                    <div>
                                        <span className='text-uppercase position-relative w-100 d-block pe-5'>
                                            HTMl
                                            <span className="position-absolute d-block bottom-2 start-100 translate-middle badge text-dark">
                                                {aboutme && aboutme.html}%
                                            </span>
                                        </span>
                                        <div style={{ height: '10px' }} className="progress mt-2" role="progressbar" aria-label="Info example" aria-valuenow={aboutme && aboutme.html} aria-valuemin="0" aria-valuemax="100">
                                            <div className="progress-bar bg-info" style={{ width: `${aboutme && aboutme.html}%`, height: '10px' }}></div>
                                        </div>
                                    </div>
                                    <div>
                                        <span className='text-uppercase position-relative w-100 d-block pe-5'>
                                            CSS
                                            <span className="position-absolute d-block bottom-2 start-100 translate-middle badge text-dark">
                                                {aboutme && aboutme.css}%
                                            </span>
                                        </span>
                                        <div style={{ height: '10px' }} className="progress mt-2" role="progressbar" aria-label="Info example" aria-valuenow={aboutme && aboutme.css} aria-valuemin="0" aria-valuemax="100">
                                            <div className="progress-bar bg-info" style={{ width: `${aboutme && aboutme.css}%`, height: '10px' }}></div>
                                        </div>
                                    </div>
                                    <div>
                                        <span className='text-uppercase position-relative w-100 d-block pe-5'>
                                            JAVASCRIPTS
                                            <span className="position-absolute d-block bottom-2 start-100 translate-middle badge text-dark">
                                                {aboutme && aboutme.js}%
                                            </span>
                                        </span>
                                            <div style={{ height: '10px' }} className="progress mt-2" role="progressbar" aria-label="Info example" aria-valuenow={aboutme && aboutme.js} aria-valuemin="0" aria-valuemax="100">
                                                <div className="progress-bar bg-info" style={{ width: `${aboutme && aboutme.js}%`, height: '10px' }}></div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        )
    }

export default About