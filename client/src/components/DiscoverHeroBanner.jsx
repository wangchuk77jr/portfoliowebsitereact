import React from 'react'

const DiscoverHeroBanner = ({ searchQuery, onSearch }) => {
  return (
    <div className='container' style={{width:'80%'}}>
        <h1 className='text-center fw-bold text-white'>Best of PORTFOLIO WEBSITE</h1>
        <h6 className='text-center fw-bold text-white'>Projects featured today by our curators</h6>
        <div className="row mt-5">
             <div className="col">
              {/* Saearch form */}
                <form action="">
                   <div className="search-group">
                    <input type="search" placeholder="Search by name"  value={searchQuery} onChange={onSearch} className="form-control h-100 ps-5 user-search-input rounded-5 bg-body-secondary border border-info"/>
                    <span class="user-search-input-icon">
                      <svg  xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" style={{fill: 'rgba(0, 0, 0, 1)'}}><path d="M10 18a7.952 7.952 0 0 0 4.897-1.688l4.396 4.396 1.414-1.414-4.396-4.396A7.952 7.952 0 0 0 18 10c0-4.411-3.589-8-8-8s-8 3.589-8 8 3.589 8 8 8zm0-14c3.309 0 6 2.691 6 6s-2.691 6-6 6-6-2.691-6-6 2.691-6 6-6z"></path></svg>
                    </span>
                  </div>
                </form>
             </div>
          </div>
    </div>
  )
}

export default DiscoverHeroBanner