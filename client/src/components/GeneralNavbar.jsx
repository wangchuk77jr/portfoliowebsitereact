import React from 'react';
import { NavLink} from 'react-router-dom';
import logo from '../assets/logo.png';

const GeneralNavbar = () => {
    const isAuthenticated = localStorage.getItem("jwtToken") !== null;
    return (
        <nav className="navbar navbar-expand-lg bg-md-transparent px-md-5 px-3 border-bottom border-white py-3">
            <div className="container-fluid">
                <div className='nav-bar-items'>
                    <NavLink style={{ height: '50px' }} className="navbar-brand d-flex align-items-center justify-content-center gap-2" to="/">
                        <img src={logo} alt="logo" />
                        <span className='text-white fw-bold pt-2 logo-text'>PORTFOLIO WEBSITE</span>
                    </NavLink>
                </div>
                <button className="navbar-toggler bg-white" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav nav-bar-items d-flex align-items-md-center justify-md-content-center ms-md-4 me-auto mb-2 mb-lg-0 gap-4">
                        <li className="nav-item">
                            <NavLink className="nav-link text-white pt-2 pe-0" aria-current="page" to="/discover">Discover</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link text-white pt-2 pe-0" to="/hire">Hire</NavLink>
                        </li>
                    </ul>
                    <ul className="navbar-nav gap-md-2 gap-3 mb-2 mb-lg-0">
                        {isAuthenticated ? (
                            <li className="nav-item">
                              <NavLink className="nav-link bg-white text-info rounded-5 px-3 fw-bold" to="/dashboard">My Dashboard</NavLink>
                            </li>
                        ) : (
                            <>
                                <li className="nav-item">
                                    <NavLink className="nav-link bg-white text-info rounded-5 px-3" to="/login">Log In</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink className="nav-link bg-info text-white rounded-5 px-3" to="/register">Sign Up</NavLink>
                                </li>
                            </>
                        )}
                    </ul>
                </div>
            </div>
        </nav>
    );
};

export default GeneralNavbar;


