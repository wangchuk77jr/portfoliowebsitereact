const express = require("express");
const cors = require("cors");
const session = require("express-session");
const bodyParser = require("body-parser");
const nodemailer = require('nodemailer');

const { registerUser, UserLogin } = require("./routes/auth");
const userRoute = require("./routes/user")

const app = express();
const PORT = process.env.PORT || 5000;

// Middleware
app.use(bodyParser.json());
app.use(cors());
app.use(express.json());
app.use("/uploads", express.static("uploads"));

app.use(bodyParser.urlencoded({ extended: false }));

// Registration route
app.post("/register", registerUser);
app.post("/login", UserLogin);
app.use(userRoute);

app.use(bodyParser.json());

// Initialize Passport and session
app.use(
  session({ secret: "your-secret-key", resave: true, saveUninitialized: true })
);

//Send Email
app.post('/api/sendEmail', async (req, res) => {
  try {
      const { email, name, sender, subject, message } = req.body;
      const transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
              user: '12220013.gcit@rub.edu.bt',
              pass: 'qcajcdqjineopfsn' 
          }
      });

      const mailOptions = {
          from: sender,
          to: email,
          subject: subject,
          text: `This mail is sent by ${name} with email: ${sender} and the message is ${message}`
      };

      // Send email
      const info = await transporter.sendMail(mailOptions);
      console.log('Email sent: ' + info.response);
      res.status(200).json({ message: 'Email sent successfully.' });
  } catch (error) {
      console.error('Error sending email:', error);
      res.status(500).json({ error: 'Internal server error' });
  }
});

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
