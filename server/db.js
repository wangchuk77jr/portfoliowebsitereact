const Pool = require("pg").Pool;

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  database: "portfilowebsitereact",
  password: "password",
  port: 5432,
});

pool.connect((err) => {
  if (err) {
    console.error("Database connection error:", err.message);
  } else {
    console.log("Connected to the psql database...");
  }
});

module.exports = { pool };
