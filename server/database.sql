-- To Create database 
CREATE DATABASE "portfoliowebsitereact";

-- Create User Table
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
	name VARCHAR(500),
    email VARCHAR(150) UNIQUE NOT NULL,
    password VARCHAR(100) NOT NULL,
    profile_bg VARCHAR(2000),
    profession VARCHAR(500)
);

-- Create About_me Table
CREATE TABLE about_me (
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id),
    name VARCHAR(200),
    phone NUMERIC,
    city VARCHAR(200),
    age NUMERIC,
    degree VARCHAR(200),
    html NUMERIC,
    css NUMERIC,
    js NUMERIC,
    profile_image VARCHAR(2000)

);

-- Create Resume Table
CREATE TABLE resume (
    resume_id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id),
    -- my_resume VARCHAR(1000),
    summary VARCHAR(1000),
    experience VARCHAR(1000),
    education VARCHAR(1000)
);

-- Create Projects Table
CREATE TABLE project (
    project_id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id),
    name VARCHAR(100) NOT NULL,
    email VARCHAR(150) NOT NULL,
    description VARCHAR(1000) NOT NULL,
    upload VARCHAR(2000) NOT NULL
);
