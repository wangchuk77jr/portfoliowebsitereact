const express = require("express");
const router = express.Router();
const multer = require("multer");
const path = require("path");
const nodemailer = require('nodemailer');

const app = express();
app.use(express.json());

const { pool } = require("../db");

// File upload configuration
const storage = multer.diskStorage({
    destination: "./uploads/",
    filename: (req, file, cb) => {
      const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
      const fileExtension = path.extname(file.originalname);
      cb(null, file.fieldname + "-" + uniqueSuffix + fileExtension);
    },
  });
  
  const upload = multer({ 
    storage,
    fileFilter: (req, file, cb) => {
      // Check if uploaded file is an image
      if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error('Only image files are allowed!'), false);
      }
      cb(null, true);
    }
  });

  ////////////////////// Edit Profile //////////////////////////////////////
router.post("/api/profile", upload.single("profile_bg"), async (req, res) => {
    try {
        const { user_id, name, profession } = req.body;  
        if (!user_id || !name || !profession || !req.file) {
            return res.status(400).json({ error: 'user_id, name, profession, and profile_bg are required!' });
        }
    
        const profile_bg = req.file.filename;
    
        // Check if user exists
        const userExistsQuery = `SELECT id FROM users WHERE id = $1`;
        const userExistsValues = [user_id];
        const userExistsResult = await pool.query(userExistsQuery, userExistsValues);
        
        if (userExistsResult.rows.length === 0) {
            return res.status(404).json({ error: 'User not found' });
        }
        
        // Update user profile
        const updateQuery = `UPDATE users SET name = $2, profession = $3, profile_bg = $4 WHERE id = $1 RETURNING *`;
        const updateValues = [user_id, name, profession, profile_bg];
    
        const result = await pool.query(updateQuery, updateValues);
        const updatedProfile = result.rows[0];
    
        // Send success message along with updated profile details
        res.status(200).json({ message: 'User profile updated successfully', user: updatedProfile });
    } catch (error) {
        console.error("Error updating user profile:", error);
        res.status(500).json({ error: "Internal server error" });
    }
});
  
// Retrieve Profiles
router.get("/api/getProfile/:userId", async (req, res) => {
  try {
      const userId = req.params.userId;

      const query = "SELECT * FROM users WHERE id = $1";

      // Execute SQL query with user ID as a parameter
      const result = await pool.query(query, [userId]);

      // Check if there's any data returned
      if (result.rows.length === 0) {
          // If no data found for the user ID, send a 404 Not Found response
          return res.status(404).json({ error: "Profile data not found for the specified user ID" });
      }

      // Retrieve about me data from query result
      const profile = result.rows[0]; // Assuming only one row for each user ID

      // Send retrieved about me data in the response
      res.status(200).json(profile);
  } catch (error) {
      console.error("Error fetching profile info:", error);
      res.status(500).json({ error: "Internal server error" });
  }
});

  ////////////////////// Add About Me //////////////////////////////////////
  // post aboutme and image
  // router.post("/api/aboutme", upload.single("profile_image"), async (req, res) => {
  //   try {
  //     const { user_id,name, phone, city, age, degree, html, css, js, freelance } = req.body;  
  //     // Check if required fields are missing
  //     if (!user_id || !name || !phone || !city || !age || !degree || !html || !css || !js || !css || !req.file) {
  //       return res.status(400).json({ error: 'Name, phone number, city, age, degree, html, css, js, freelance and profile image are required!' });
  //     }
  
  //     const profile_image = req.file.filename;
  
  //     const query = `INSERT INTO about_me (user_id,name, phone, city, age, degree, html, css, js, freelance, profile_image) 
  //                    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING user_id,name, phone, city, age, degree, html, css, js, freelance, profile_image`;
  //     const values = [user_id, name, phone, city, age, degree, html, css, js,freelance, profile_image];
  
  //     const result = await pool.query(query, values);
  //     const createdAboutme = result.rows[0];
  
  //     // Send success message along with created aboutme details
  //     res.status(201).json({ message: 'About me created successfully', about_me: createdAboutme });
  //   } catch (error) {
  //     console.error("Error creating about_me:", error);
  //     res.status(500).json({ error: "Internal server error" });
  //   }
  // });  
  router.post("/api/aboutme", upload.single("profile_image"), async (req, res) => {
    try {
        const { user_id, name, phone, city, age, degree, html, css, js, freelance } = req.body;

        // Check if required fields are missing
        if (!user_id || !name || !phone || !city || !age || !degree || !html || !css || !js || !freelance || !req.file) {
            return res.status(400).json({ error: 'Name, phone number, city, age, degree, html, css, js, freelance, and profile image are required!' });
        }

        const profile_image = req.file.filename;

        // Check if user_id already exists in the database
        const existingRecord = await pool.query("SELECT * FROM about_me WHERE user_id = $1", [user_id]);

        if (existingRecord.rows.length > 0) {
            // If user_id exists, update the existing record
            const query = `UPDATE about_me 
                           SET name = $2, phone = $3, city = $4, age = $5, degree = $6, html = $7, css = $8, js = $9, freelance = $10, profile_image = $11
                           WHERE user_id = $1 RETURNING user_id, name, phone, city, age, degree, html, css, js, freelance, profile_image`;
            const values = [user_id, name, phone, city, age, degree, html, css, js, freelance, profile_image];

            const result = await pool.query(query, values);
            const updatedAboutme = result.rows[0];

            // Send success message along with updated aboutme details
            res.status(200).json({ message: 'About me updated successfully', about_me: updatedAboutme });
        } else {
            // If user_id does not exist, add a new record
            const query = `INSERT INTO about_me (user_id, name, phone, city, age, degree, html, css, js, freelance, profile_image) 
                           VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING user_id, name, phone, city, age, degree, html, css, js, freelance, profile_image`;
            const values = [user_id, name, phone, city, age, degree, html, css, js, freelance, profile_image];

            const result = await pool.query(query, values);
            const createdAboutme = result.rows[0];

            // Send success message along with created aboutme details
            res.status(201).json({ message: 'About me created successfully', about_me: createdAboutme });
        }
    } catch (error) {
        console.error("Error creating or updating about_me:", error);
        res.status(500).json({ error: "Internal server error" });
    }
});

 
// Retrieve About me  
router.get("/api/getAboutme/:userId", async (req, res) => {
  try {
      const userId = req.params.userId;

      const query = "SELECT * FROM about_me WHERE user_id = $1";

      // Execute SQL query with user ID as a parameter
      const result = await pool.query(query, [userId]);

      // Check if there's any data returned
      if (result.rows.length === 0) {
          // If no data found for the user ID, send a 404 Not Found response
          return res.status(404).json({ error: "About me data not found for the specified user ID" });
      }

      // Retrieve about me data from query result
      const aboutMe = result.rows[0]; // Assuming only one row for each user ID

      // Send retrieved about me data in the response
      res.status(200).json(aboutMe);
  } catch (error) {
      console.error("Error fetching about me:", error);
      res.status(500).json({ error: "Internal server error" });
  }
});

////////////////////// Add resume //////////////////////////////////////
// router.post("/api/resume", upload.none(), async (req, res) => {
//   try {
//       const { user_id, summary, experience, education } = req.body;
//       console.log(req.body);
//       // Check if required fields are missing
//       if (!user_id || !summary || !experience || !education) {
//           return res.status(400).json({ error: 'User ID, summary, experience, and education are required!' });
//       }
      
//       const query = `INSERT INTO resume (user_id, summary, experience, education) 
//                      VALUES ($1, $2, $3, $4) 
//                      RETURNING user_id, summary, experience, education`;
//       const values = [user_id, summary, experience, education];
      
//       const result = await pool.query(query, values);
//       const createdResume = result.rows[0];
      
//       // Send success message along with created resume details
//       res.status(201).json({ message: 'Resume created successfully', resume: createdResume });
//   } catch (error) {
//       console.error("Error creating resume:", error);
//       res.status(500).json({ error: "Internal server error" });
//   }
// });

router.post("/api/resume", upload.none(), async (req, res) => {
  try {
    const { user_id, summary, experience, education } = req.body;
    if (!user_id || !summary || !experience || !education) {
        return res.status(400).json({ error: 'User ID, summary, experience, and education are required!' });
    }
    
      // Check if user exists
      const existingRecord = await pool.query("SELECT * FROM resume WHERE user_id = $1", [user_id]);

      if (existingRecord.rows.length > 0) {
          // If user_id exists, update the existing record
          const query = `UPDATE resume 
                         SET summary = $2, experience = $3, education = $4
                         WHERE user_id = $1 RETURNING user_id, summary, experience, education`;
          const values = [user_id, summary, experience, education];

          const result = await pool.query(query, values);
          const updatedResume = result.rows[0];

          // Send success message along with updated aboutme details
          res.status(200).json({ message: 'Resume updated successfully', resume: updatedResume });
      } else {
          // If user_id does not exist, add a new record
          const query = `INSERT INTO resume (user_id, summary, experience, education) 
                         VALUES ($1, $2, $3, $4) RETURNING user_id, summary, experience, education`;
          const values = [user_id, summary, experience, education];

          const result = await pool.query(query, values);
          const createdResume = result.rows[0];

          // Send success message along with created aboutme details
          res.status(201).json({ message: 'Resume created successfully', about_me: createdResume });
      }
  } catch (error) {
      console.error("Error creating or updating resume:", error);
      res.status(500).json({ error: "Internal server error" });
  }
});

//Retrieve Resume
router.get("/api/getResume/:userId", async (req, res) => {
  try {
      const userId = req.params.userId;

      const query = "SELECT * FROM resume WHERE user_id = $1";

      const result = await pool.query(query, [userId]);

      if (result.rows.length === 0) {
          return res.status(404).json({ error: "Resume data not found for the specified user ID" });
      }

      const resume = result.rows[0]; 

      res.status(200).json(resume);
  } catch (error) {
      console.error("Error fetching resume:", error);
      res.status(500).json({ error: "Internal server error" });
  }
});

///////////////////////////////// Add Portfolio //////////////////////////
router.post("/api/portfolio", upload.single("upload"), async (req, res) => {
  try {
    const { user_id, name, email, description } = req.body;  
    // Check if required fields are missing
    if (!user_id || !name || !email || !description || !req.file) {
      return res.status(400).json({ error: 'user_id, name, email, description and upload are required!' });
    }

    const upload = req.file.filename;

    const query = `INSERT INTO project (user_id, name, email, description, upload) 
                   VALUES ($1, $2, $3, $4, $5 ) RETURNING user_id, name, email, description, upload`;
    const values = [user_id, name, email, description, upload];

    const result = await pool.query(query, values);
    const createdproject = result.rows[0];

    // Send success message along with created aboutme details
    res.status(201).json({ message: 'Portfolio created successfully', project: createdproject });
  } catch (error) {
    console.error("Error creating portfolio:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});  

// Retrieve all users route (GET)
router.get("/api/getPortfolio/:userId", async (req, res) => {
  try {
    const userId = req.params.userId;

    const query = "SELECT * FROM project WHERE user_id = $1";

    const result = await pool.query(query, [userId]);

    if (result.rows.length === 0) {
      return res.status(404).json({ error: "Portfolio data not found for the specified user ID" });
    }

    const portfolio = result.rows;

    res.status(200).json(portfolio);
  } catch (error) {
    console.error("Error fetching portfolio:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

//////////////////////////////////// ALL PROJECT IN USER SIDE ///////////////////////////
router.get("/api/getProject", async (req, res) => {
  try {
    const query = "SELECT p.*, u.name AS author FROM project p JOIN users u ON p.user_id = u.id";

    const result = await pool.query(query);

    const project = result.rows;

    res.status(200).json({ project });
  } catch (error) {
    console.error("Error fetching project:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

////////////////////////////////// Email ///////////////////////////
router.get("/api/getMail/:userId", async (req, res) => {
  try {
    const userId = req.params.userId;
    const query = "SELECT email FROM users WHERE id = $1";
    const result = await pool.query(query, [userId]);

    if (result.rows.length === 0) {
      return res.status(404).json({ error: "User not found for the specified user ID" });
    }
    const email = result.rows[0].email;
    res.status(200).json({ email });
  } catch (error) {
    console.error("Error fetching user email:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

// get hire
router.get("/api/getHire", async (req, res) => {
  try {
const query = `
    SELECT u.*, 
           (SELECT COUNT(*) FROM project p WHERE u.id = p.user_id) AS complete
    FROM users u;
`;

    const result = await pool.query(query);

    const hire = result.rows;
  
    res.status(200).json({ hire });
  } catch (error) {
    console.error("Error fetching hire:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;